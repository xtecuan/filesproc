/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.xtecuan.fileprocessor;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author xtecuan
 */
public class ProcessFile extends SimpleFileVisitor<Path> {

    public static final String REPORT_FILE = "/home/xtecuan/Desktop/Archivos.txt";
    private static List<File> files = new ArrayList<>(0);
    private static List<File> dirs = new ArrayList<>(0);
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
    private static SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy kk:mm:ss");
    private static final int LIMIT_YEAR = 2013;

    public ProcessFile() {

    }

    @Override
    public FileVisitResult visitFile(Path t, BasicFileAttributes bfa) throws IOException {

        File current = t.toFile();

        System.out.println("Processing File: " + current.getPath());

        Date modified = new Date(current.lastModified());
        int year = Integer.valueOf(sdf.format(modified));

        if (year <= LIMIT_YEAR) {
            System.out.println("Adding File...");
            files.add(current);
        }

        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult preVisitDirectory(Path t, BasicFileAttributes bfa) throws IOException {

        File current = t.toFile();

        System.out.println("Processing Directory: " + current.getPath());

        Date modified = new Date(current.lastModified());
        int year = Integer.valueOf(sdf.format(modified));

        if (year <= LIMIT_YEAR) {
            System.out.println("Adding Directory...");
            dirs.add(current);
        }
        return FileVisitResult.CONTINUE;
    }

    public static int getFilesSize() {
        return files.size();
    }

    public static int getDirsSize() {
        return dirs.size();
    }

    private static String formatLong(long time) {

        return sdf1.format(new Date(time));
    }

    public static String getStringSizeLengthFile(long size) {

        DecimalFormat df = new DecimalFormat("0.00");

        float sizeKb = 1024.0f;
        float sizeMo = sizeKb * sizeKb;
        float sizeGo = sizeMo * sizeKb;
        float sizeTerra = sizeGo * sizeKb;

        if (size < sizeMo) {
            return df.format(size / sizeKb) + " Kb";
        } else if (size < sizeGo) {
            return df.format(size / sizeMo) + " Mb";
        } else if (size < sizeTerra) {
            return df.format(size / sizeGo) + " Gb";
        }

        return "";
    }

    public static void writeReport(String path, String reportPath) throws IOException {

        File report = new File(reportPath);

        PrintWriter fw = new PrintWriter(report);
        fw.println("================================================================================");
        fw.println("Path Scanned: " + path + " generated at: " + sdf1.format(new Date()) + "");
        fw.println("================================================================================");
        fw.println("List of directories previous " + LIMIT_YEAR + " size: (" + dirs.size() + ") :");

        for (File dir : dirs) {
            fw.println(dir.getPath() + "\t" + formatLong(dir.lastModified()) + "\t" + getStringSizeLengthFile(dir.length()));
        }
        fw.println("================================================================================");
        fw.println("List of files previous " + LIMIT_YEAR + " size: (" + files.size() + ") :");

        for (File file : files) {
            fw.println(file.getPath() + "\t" + formatLong(file.lastModified()) + "\t" + getStringSizeLengthFile(file.length()));
        }

        fw.flush();
        fw.close();

        System.out.println("Report in: " + report.getPath());
    }

}
