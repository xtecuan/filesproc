/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.xtecuan.fileprocessor;

import java.io.IOException;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 *
 * @author xtecuan
 */
public class FileReporter {

    private static final String Start = "/home/xtecuan/Documentos/";

    public static void main1(String[] args) throws IOException {

        FileVisitor<Path> fileProcessor = new ProcessFile();
        Files.walkFileTree(Paths.get(Start), fileProcessor);

        ProcessFile.writeReport(Start,ProcessFile.REPORT_FILE);

    }

    public static void main(String[] args) throws IOException {

        if (args.length == 2) {

            FileVisitor<Path> fileProcessor = new ProcessFile();
            Files.walkFileTree(Paths.get(args[0]), fileProcessor);

            ProcessFile.writeReport(args[0],args[1]);

        } else {
            System.out.println("Usage: java -jar FilesProc.jar Directory_To_Process PathAndFileToReport");
        }

    }
}
